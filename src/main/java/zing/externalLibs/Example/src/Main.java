package zing.externalLibs.Example.src;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.istack.Nullable;
import zing.externalLibs.src.GetDocumentCallback;
import zing.externalLibs.src.OrangeRequest;
import zing.externalLibs.src.PostBillCallback;
import zing.externalLibs.src.constants.DocumentType;
import zing.externalLibs.src.constants.PaymentType;
import zing.externalLibs.src.constants.Tax;
import zing.externalLibs.src.constants.TaxationSystem;
import zing.externalLibs.src.models.*;

import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;

public class Main {


    public static void main(String[] args) {

        postData();
        //getData();
    }

    public static void getData(CompletableFuture<DocumentState> completableFuture) {

        OrangeRequest orangeRequest = new OrangeRequest();
        orangeRequest.getDocument("newId2", "7725713770", new GetDocumentCallback() {
            @Override
            public void onSuccess(DocumentState documentState) {
                Gson gson = new GsonBuilder().disableHtmlEscaping().create();
                System.out.println("Success! " + documentState.getCompanyName());

                System.err.println(gson.toJson(documentState));
                completableFuture.complete(documentState);
            }

            @Override
            public void onRequestFailure(Throwable throwable) {
                System.out.println(throwable.getMessage());
            }
        });

    }

    private static void postData() {
        try {
            OrangeRequest orangeRequest = new OrangeRequest();
            orangeRequest.postDocument(getDummy(), "C:\\Users\\frees\\Downloads\\File_for_test\\private_key.der", new PostBillCallback() {
                @Override
                public void onSuccess() {
                    System.out.println("onSuccess");
                    getData(new CompletableFuture<DocumentState>().whenComplete(new BiConsumer<DocumentState, Throwable>() {
                        @Override
                        public void accept(DocumentState s, Throwable throwable) {
                            System.err.println(s);
                        }
                    }));
                }

                @Override
                public void onValidationErrors(@Nullable String[] errors) {
                    if (errors != null) {
                        for (String error : errors) {
                            System.out.println("onValidationErrors: " + error);
                        }
                    } else {
                        System.out.println("postResponse is null");
                    }
                }

                @Override
                public void onRequestFailure(Throwable throwable) {
                    System.out.println("onRequestFailure " + throwable.getMessage());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static RequestBody getDummy() {
        Bill dummyBill = new Bill(
                1.0,
                1.0,
                Tax.NONE,
                "Dummy text");
        Payment dummyPayment = new Payment(
                PaymentType.CARD_VISA,
                1.0
        );
        CheckClose dummyCheckClose = new CheckClose(
                Collections.singletonList(dummyPayment),
                TaxationSystem.USN_INCOME
        );
        Document dummyDocument = new Document(
                DocumentType.INCOME,
                Collections.singletonList(dummyBill),
                dummyCheckClose,
                "Dummy customer contact"
        );

        return new RequestBody(
                "newId2",
                "7725713770",
                null,
                dummyDocument
        );
    }
}
