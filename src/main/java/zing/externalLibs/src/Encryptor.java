package zing.externalLibs.src;

import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;

/**
 * Created by Alexey Padyukov on 19.06.2017.
 */

public class Encryptor {

    public static PrivateKey getRSAKey() throws IOException {
        byte[] modulusBytes = Base64.decodeBase64("3FxO0OZ4k0aKswSRuzsBHuF8fJtjJ6ErCpiQFTiTesRAehPy6hSbAKOvUSVpoxvl9pL0zj2PJKw7v2Hah7+ycVV/reTBKQ9K5V/AEQOSMgjT8Yz/cnLvVEPnrNOiqi/E7YUXv51szUUIVNhxumm3kKH1zpbaIKpotf4xRUNsMd5DoYEHI1gpZ96TVkdUIZWjKXCeWPdT2cXIybaejkKsQtyG0X3rMQRTnTYyFXka8tasia9rrUR8FzGZibUyvDKePQNCrExeFrWqgRLGZ9DikuzaUi7ilcHKgM2aJgbU8PGFAYCaBFlCnteqqBJALL3SnhiaE+4WdmGf5ED2/NoZQQ==");
        byte[] publicExpBytes = Base64.decodeBase64("AQAB");
        BigInteger modulus = new BigInteger(1, modulusBytes);
        BigInteger publicExp = new BigInteger(1, publicExpBytes);
        RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(modulus, publicExp);
        KeyFactory factory = null;
        try {
            factory = KeyFactory.getInstance("RSA");
            PrivateKey priv = factory.generatePrivate(keySpec);
            return priv;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static byte[] signData(byte[] data, String derKeyPath) throws IOException {
        Signature signature = null;
        try {
            signature = Signature.getInstance("SHA256withRSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            signature.initSign(get(derKeyPath));
            signature.update(data);
            return signature.sign();
        } catch (InvalidKeyException | SignatureException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static PrivateKey get(String filename) throws IOException {

        byte[] keyBytes = Files.readAllBytes(Paths.get(filename));

        PKCS8EncodedKeySpec spec =
                new PKCS8EncodedKeySpec(keyBytes);
        try {
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePrivate(spec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return null;
    }

}
