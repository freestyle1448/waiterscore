package zing.exception;

import com.mongodb.MongoException;

public class NotFoundException extends MongoException {
    public NotFoundException(String message) {
        super(message);
    }
}