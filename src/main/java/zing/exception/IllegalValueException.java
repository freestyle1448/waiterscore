package zing.exception;

import com.mongodb.MongoException;

public class IllegalValueException extends MongoException {
    public IllegalValueException(String message) {
        super(message);
    }
}
