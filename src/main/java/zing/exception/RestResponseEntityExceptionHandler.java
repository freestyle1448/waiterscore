package zing.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Arrays;

@ControllerAdvice
public class RestResponseEntityExceptionHandler
        extends ResponseEntityExceptionHandler {

    @ExceptionHandler
    protected ResponseEntity<Object> handleConflict(
            Exception ex, WebRequest request) {
        StringBuilder bodyOfResponse = new StringBuilder();
        bodyOfResponse.append(ex.getClass()).append(" ").append(ex.getMessage()).append("\n\n");

        if (ex.getStackTrace().length > 20) {
            for (int i = 0; i < 20; i++) {
                StackTraceElement element = ex.getStackTrace()[i];
                bodyOfResponse.append(element.getClassName()).append(".").append(element.getMethodName()).append("() on line ").append(element.getLineNumber()).append("\n");
            }
        } else {
            for (int i = 0; i < ex.getStackTrace().length; i++) {
                StackTraceElement element = ex.getStackTrace()[i];
                bodyOfResponse.append(element.getClassName()).append(".").append(element.getMethodName()).append("() on line ").append(element.getLineNumber()).append("\n");
            }
        }

        System.err.println(Arrays.toString(ex.getStackTrace()));

        return handleExceptionInternal(ex, bodyOfResponse.toString(),
                new HttpHeaders(), HttpStatus.CONFLICT, request);
    }
}

