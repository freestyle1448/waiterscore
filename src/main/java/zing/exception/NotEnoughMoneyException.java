package zing.exception;

import com.mongodb.MongoException;

public class NotEnoughMoneyException extends MongoException {
    public NotEnoughMoneyException(String message) {
        super(message);
    }
}
