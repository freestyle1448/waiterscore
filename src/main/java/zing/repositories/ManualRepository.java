package zing.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import zing.models.Account;
import zing.models.Gate;
import zing.models.transaction.Balance;

@Repository
public class ManualRepository {
    private final MongoTemplate mongoTemplate;

    public ManualRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public Account findAndModifyAccountSub(ObjectId accountId, Balance balance) {
        Query findAndModifyAccount = new Query(Criteria
                .where("_id").is(accountId)
                .and("balance.amount").gte(balance.getAmount())
                .and("balance.currency").is(balance.getCurrency()));
        Update update = new Update();
        update.inc("balance.amount", -balance.getAmount().longValue());

        return mongoTemplate.findAndModify(findAndModifyAccount, update, Account.class);
    }

    public Account findAndModifyAccountAdd(String accountId, Balance balance) {
        Query findAndModifyAccount = new Query(Criteria
                .where("accountId").is(accountId)
                .and("balance.currency").is(balance.getCurrency()));
        Update update = new Update();
        update.inc("balance.amount", balance.getAmount().longValue());

        return mongoTemplate.findAndModify(findAndModifyAccount, update, Account.class);
    }

    public Gate findAndModifyGateAdd(ObjectId gateId, Balance balance) {
        Query findAndModifyGate = new Query(Criteria.where("_id").is(gateId)
                .and("balance.currency").is(balance.getCurrency())
                .and("balance.amount").gte(balance.getAmount().longValue()));
        Update update = new Update();
        update.inc("balance.amount", balance.getAmount().longValue());

        return mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }

    public Gate findAndModifyGateSub(ObjectId gateId, Balance balance) {
        Query findAndModifyGate = new Query(Criteria
                .where("_id").is(gateId)
                .and("balance.amount").gte(balance.getAmount())
                .and("balance.currency").is(balance.getCurrency()));
        Update update = new Update();
        update.inc("balance.amount", -balance.getAmount().longValue());

        return mongoTemplate.findAndModify(findAndModifyGate, update, Gate.class);
    }
}
