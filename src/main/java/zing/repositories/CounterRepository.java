package zing.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import zing.models.Counter;

public interface CounterRepository extends MongoRepository<Counter, ObjectId> {
}
