package zing.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import zing.models.Log;

public interface LogsRepository extends MongoRepository<Log, ObjectId> {
}
