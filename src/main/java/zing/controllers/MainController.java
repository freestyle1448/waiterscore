package zing.controllers;

import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import zing.exception.IllegalValueException;
import zing.exception.NotFoundException;
import zing.externalLibs.src.models.DocumentState;
import zing.models.Gate;
import zing.models.payture.InitRequestData;
import zing.models.payture.PaytureCredentials;
import zing.models.payture.SessionType;
import zing.models.psb.PSBCredentials;
import zing.models.transaction.*;
import zing.repositories.GatesRepository;
import zing.repositories.TransactionsRepository;
import zing.services.UserService;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

@SuppressWarnings("MVCPathVariableInspection")
@RestController
public class MainController {
    private final UserService userService;
    private final GatesRepository gatesRepository;
    private final TransactionsRepository transactionsRepository;

    public MainController(UserService userService, GatesRepository gatesRepository, TransactionsRepository transactionsRepository) {
        this.userService = userService;
        this.gatesRepository = gatesRepository;
        this.transactionsRepository = transactionsRepository;
    }

    @PostMapping("/withdraw/{gateId}")
    public Callable<Transaction> withdraw(TransactionDTO transactionDTO) {
        Transaction transaction = transactionDTO.createTransaction(Type.WITHDRAWAL);
        Optional<Gate> gateOptional = gatesRepository.findById(transaction.getGateId());

        if (gateOptional.isPresent()) {
            Gate gate = gateOptional.get();
            String currency = gate.getBalance().getCurrency();
            transaction.getFinalAmount().setCurrency(currency);
            transaction.getCommission().setCurrency(currency);
            transaction.getAmount().setCurrency(currency);

            transaction.setTrtype("70");
            transaction.setGateType(gate.getType());

            if (transaction.getAmount().getAmount().longValue() < 0 || transaction.getFinalAmount().getAmount().longValue() < 0)
                throw new IllegalValueException("Суммы не могут быть меньше 0!");
            return () -> userService.withdraw(transaction).get();
        } else {
            throw new NotFoundException("Гейт с указанным ID не найден!");
        }
    }

    @PostMapping("/input")
    public Callable<String> input(InitRequestData initRequestData, Long Commission) {
        Optional<Gate> gateOptional = gatesRepository.findById(PaytureCredentials.PAYTURE_GATE_ID);
        if (gateOptional.isEmpty())
            throw new NotFoundException("Payture гейт не найден!");
        Gate gate = gateOptional.get();
        initRequestData.setUrl(gate.getSuccessUrl());

        if (Commission == null)
            Commission = 0L;
        if (initRequestData.getTotal() == null)
            initRequestData.setTotal(String.valueOf((double) (Long.parseLong(initRequestData.getAmount()) + Commission) / 100));
        initRequestData.setAmount(String.valueOf(Long.parseLong(initRequestData.getAmount()) + Commission));
        initRequestData.setSessionType(SessionType.PAY);
        initRequestData.setOrderId(UUID.randomUUID().toString());

        Long finalCommission = Commission;
        return () -> userService.input(initRequestData, finalCommission).get();
    }

    @PostMapping("/input_psb")
    public Callable<String> inputPsb(TransactionDTO transactionDTO) {
        final Transaction transaction = transactionDTO.createTransaction(Type.DONATE);

        final Optional<Gate> gateOptional = gatesRepository.findById(PSBCredentials.GATE_ID_DONATE);
        if (gateOptional.isEmpty())
            throw new NotFoundException("Гейт ПСБ не найден!");
        final Gate gate = gateOptional.get();
        transaction.setAmount(Balance.builder()
                .amount(transaction.getAmount().getAmount())
                .currency(gate.getBalance().getCurrency())
                .build());
        transaction.setFinalAmount(Balance.builder()
                .amount(transaction.getFinalAmount().getAmount())
                .currency(gate.getBalance().getCurrency())
                .build());
        transaction.setOrderId(String.valueOf(transaction.getTransactionNumber()));
        transaction.setStatus(Status.IN_PROCESS);
        transaction.setGateId(PSBCredentials.GATE_ID_DONATE);
        transaction.setGateType(gate.getType());
        transaction.setTrtype("1");
        transaction.setStage(2);

        return () -> userService.inputPsb(transaction).get();
    }

    @GetMapping("/get_cheque_status")
    public Callable<DocumentState> genCheque(String transactionId) {
        final var stringCompletableFuture = new CompletableFuture<DocumentState>();

        final var transactionOptional = transactionsRepository.findById(new ObjectId(transactionId));
        if (transactionOptional.isEmpty())
            throw new NotFoundException("Транзакция с указанным ID не найдена!");
        final var transaction = transactionOptional.get();
        if (transaction.getIsChequeGenerated() != null) {
            if (transaction.getIsChequeGenerated())
                userService.getChequeData(transaction, stringCompletableFuture);
            else {
                transaction.setIsChequeGenerated(true);
                userService.generateCheque(transaction, stringCompletableFuture);
            }
        } else {
            transaction.setIsChequeGenerated(true);
            userService.generateCheque(transaction, stringCompletableFuture);
        }

        return stringCompletableFuture::get;
    }
}
