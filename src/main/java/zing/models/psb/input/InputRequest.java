package zing.models.psb.input;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class InputRequest {
    private String AMOUNT;
    @Builder.Default
    private String CURRENCY = "RUB";
    private String ORDER;
    private String DESC;
    private String TERMINAL;
    @Builder.Default
    private String TRTYPE = "1";
    private String MERCH_NAME;
    private String MERCHANT;
    private String EMAIL;
    private String TIMESTAMP;
    private String NONCE;
    @Builder.Default
    private String BACKREF = "https://zingpay.ru";
    private String NOTIFY_URL;
    private String P_SIGN;
}
