package zing.models.psb;

final class Result {
    public static final String SUCCESS = "0";
    public static final String REPEAT = "1";
    public static final String BANK_DENIED = "2";
    public static final String SENDER_DENIED = "3";
}
