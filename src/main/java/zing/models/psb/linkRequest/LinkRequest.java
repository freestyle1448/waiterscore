package zing.models.psb.linkRequest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LinkRequest {
    private String AMOUNT;
    @Builder.Default
    private String CURRENCY = "RUB";
    private String DESC;
    private String TERMINAL;
    @Builder.Default
    private String TRTYPE = "1";
    private String BACKREF;
    private String ORDER;
    private String EMAIL;
    @Builder.Default
    private String MERCHANT_NOTIFY_EMAIL = "freestyle1448@gmail.com";
    @Builder.Default
    private String ADDINFO = "оплата";
    private String NOTIFY_URL;
    private String P_SIGN;

    public void createSign() {
        StringBuilder signBuilder = new StringBuilder();
        signBuilder.append(AMOUNT != null && !AMOUNT.isEmpty() ? String.format("%d%s", AMOUNT.length(), AMOUNT) : "-");
        signBuilder.append(CURRENCY != null && !CURRENCY.isEmpty() ? String.format("%d%s", CURRENCY.length(), CURRENCY) : "-");
        signBuilder.append(TERMINAL != null && !TERMINAL.isEmpty() ? String.format("%d%s", TERMINAL.length(), TERMINAL) : "-");
        signBuilder.append(TRTYPE != null && !TRTYPE.isEmpty() ? String.format("%d%s", TRTYPE.length(), TRTYPE) : "-");
        signBuilder.append(BACKREF != null && !BACKREF.isEmpty() ? String.format("%d%s", BACKREF.length(), BACKREF) : "-");
        signBuilder.append(ORDER != null && !ORDER.isEmpty() ? String.format("%d%s", ORDER.length(), ORDER) : "-");

        this.P_SIGN = genHash(signBuilder.toString());
    }

    private String genHash(String stringToHash) {
        Process process;

        try {
            process = Runtime.getRuntime().exec(new String[]{"php", "/opt/tomcat/latest/webapps/sign.php", "34833044E2AA7E0039A42517119DE664", stringToHash});

            process.waitFor();

            String line;
            String hash = null;

            BufferedReader outputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

            while ((line = outputReader.readLine()) != null) {
                hash = line.trim();
            }

            outputReader.close();
            return hash;

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }

    public MultiValueMap<String, String> requestData() throws IllegalAccessException {
        MultiValueMap<String, String> map =
                new LinkedMultiValueMap<>();

        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.get(this) == null)
                continue;
            map.add(field.getName(), String.valueOf(field.get(this)));
        }

        return map;
    }
}
