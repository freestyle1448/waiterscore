package zing.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import zing.models.transaction.Balance;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "gates")
public
class Gate {
    public final static String COLLECTION_NAME = "gates";

    @Id
    private ObjectId id;
    private String name;
    private Integer type;
    private CommissionFull commission;
    private Balance balance;
    private String account;
    private String merchantKey;
    private String currentUrlInit;
    private String currentUrlPayStatus;
    private String successUrl;
    private String terminal;
    private String merchantName;
    private String merchant;
    private String urlPayment;
    private String genLinkUrl;
    private String urlWithdraw;
}
