package zing.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import zing.models.transaction.Balance;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Document(collection = "accounts")
public
class Account {
    public final static String COLLECTION_NAME = "accounts";

    @Id
    private ObjectId id;
    private Balance balance;
    private ObjectId userId;
    private String accountId;
}