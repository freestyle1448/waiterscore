package zing.models.payture;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CheckClose {
    @Builder.Default
    private Integer TaxationSystem = 0;
}
