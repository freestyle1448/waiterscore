package zing.models.payture;

import lombok.*;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InitRequestData {
    private String accountId;
    @NonNull
    private String SessionType;
    @NonNull
    private String OrderId;
    @NonNull
    private String Amount;
    private String IP;
    private String success_url;
    private String Url;
    private String Description;
    private String TemplateTag;
    private String Language;
    @Builder.Default
    private String Product = PaytureCredentials.PRODUCT;
    private String Total;
    private String Cheque;

    public String returnUrlEncoded() throws IllegalAccessException, UnsupportedEncodingException {
        StringBuilder stringBuffer = new StringBuilder();
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.getName().equals("receiverCredentials") || field.get(this) == null || field.getName().equals("accountId"))
                continue;
            if (field.getName().equals("success_url")) {
                stringBuffer.append(String.format("Url=%s;", field.get(this)));
                continue;
            }
            stringBuffer.append(String.format("%s=%s;", field.getName(), field.get(this)));
        }

        return URLEncoder.encode(stringBuffer.toString(), StandardCharsets.UTF_8.toString());
    }
}
