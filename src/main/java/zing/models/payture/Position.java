package zing.models.payture;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Position {
    private Double Price;
    @Builder.Default
    private Integer Quantity = 1;
    @Builder.Default
    private Integer Tax = 6;
    @Builder.Default
    private String Text = "Оплата чаевых";
}
