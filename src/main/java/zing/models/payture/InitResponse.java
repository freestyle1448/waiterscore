package zing.models.payture;

import lombok.*;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

@XmlRootElement(name = "Init")
public class InitResponse {
    @NonNull
    @XmlAttribute(name = "Success")
    private String Success;
    @NonNull
    @XmlAttribute(name = "OrderId")
    private String OrderId;
    @NonNull
    @XmlAttribute(name = "Forwarded")
    private Boolean Forwarded;
    @XmlAttribute(name = "MerchantContract")
    private String MerchantContract;
    @XmlAttribute(name = "FinalTerminal")
    private String FinalTerminal;
    @XmlAttribute(name = "Amount")
    private Long Amount;
    @XmlAttribute(name = "SessionId")
    private String SessionId;
    @XmlAttribute(name = "State")
    private String State;
    @XmlAttribute(name = "ErrCode")
    private String ErrCode;
}
