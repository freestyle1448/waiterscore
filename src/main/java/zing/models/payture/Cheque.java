package zing.models.payture;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Cheque {
    private List<Position> Positions;
    @Builder.Default
    private String CustomerContact = "cheque@zingtogether.com";
    private CheckClose CheckClose;
}
