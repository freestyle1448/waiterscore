package zing.models.payture;

public final class SessionType {
    public static final String PAY = "Pay";
    public static final String BLOCK = "Block";
}
