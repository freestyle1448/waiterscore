package zing.models.transaction;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import zing.models.Partner;
import zing.utils.GsonUtils;

import java.io.StringReader;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TransactionDTO {
    private final Gson gson = GsonUtils.getGson();

    private String account_id;
    private String gateId;
    private String amount;
    private String commission;
    private String finalAmount;
    private String systemRate;
    private String note;
    private String receiverCredentials;
    private String senderCredentials;
    private String sign;
    private String salt;
    private String partner;
    private String successRef;

    public Transaction createTransaction(String type) {
        JsonReader receiverReader = new JsonReader(new StringReader(receiverCredentials));
        receiverReader.setLenient(true);
        JsonReader senderReader = new JsonReader(new StringReader(senderCredentials));
        senderReader.setLenient(true);

        return Transaction.builder()
                .sign(sign)
                .salt(salt)
                .gateId(gateId != null ? new ObjectId(gateId) : null)
                .startDate(new Date())
                .transactionNumber(System.currentTimeMillis())
                .status(Status.WAITING)
                .stage(0)
                .type(type)
                .amount(amount != null ? Balance.builder()
                        .amount(Long.valueOf(amount))
                        .build() : null)
                .commission(commission != null ? gson.fromJson(commission, TransactionCommission.class) : TransactionCommission.builder()
                        .bankAmount(0L)
                        .systemAmount(0L)
                        .currency("RUB")
                        .build())
                .finalAmount(finalAmount != null ? Balance.builder()
                        .amount(Long.valueOf(finalAmount))
                        .build() : Balance.builder()
                        .amount(Long.valueOf(amount))
                        .build())
                .purpose(note)
                .receiverCredentials(receiverCredentials != null ? gson.fromJson(receiverReader, ReceiverCredentials.class) : null)
                .senderCredentials(senderCredentials != null ? gson.fromJson(senderReader, SenderCredentials.class) : null)
                .partner(partner != null ? gson.fromJson(partner, Partner.class) : null)
                .successRef(successRef)
                .build();
    }
}
