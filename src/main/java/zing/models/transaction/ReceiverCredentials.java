package zing.models.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReceiverCredentials {
    private ObjectId accountId;
    private String senderEmail;
    private String cardNumber;
    private String backref;
    private String userCredentials;
}
