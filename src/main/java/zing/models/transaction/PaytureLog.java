package zing.models.transaction;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PaytureLog {
    private String payStatusResponse;
    private String payStatusRequest;
    private String initRequest;
    private String initResponse;
}