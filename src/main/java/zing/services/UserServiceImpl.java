package zing.services;

import com.google.gson.Gson;
import com.sun.istack.Nullable;
import org.bson.types.ObjectId;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import zing.exception.NotEnoughMoneyException;
import zing.exception.NotFoundException;
import zing.externalLibs.src.GetDocumentCallback;
import zing.externalLibs.src.OrangeRequest;
import zing.externalLibs.src.PostBillCallback;
import zing.externalLibs.src.constants.DocumentType;
import zing.externalLibs.src.constants.PaymentType;
import zing.externalLibs.src.constants.Tax;
import zing.externalLibs.src.constants.TaxationSystem;
import zing.externalLibs.src.models.*;
import zing.models.Account;
import zing.models.Counter;
import zing.models.Gate;
import zing.models.payture.CheckClose;
import zing.models.payture.*;
import zing.models.psb.PSBCredentials;
import zing.models.psb.linkRequest.LinkRequest;
import zing.models.psb.linkRequest.LinkResponse;
import zing.models.transaction.*;
import zing.repositories.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;

import static zing.models.Constants.KEYS_PATH;
import static zing.models.Constants.ZING_INN;
import static zing.models.transaction.Type.DONATE;

@Service
public class UserServiceImpl implements UserService {
    private final TransactionsRepository transactionsRepository;
    private final ManualRepository manualRepository;
    private final AccountsRepository accountsRepository;
    private final GatesRepository gatesRepository;
    private final CounterRepository counterRepository;
    private final RestTemplate restTemplate;

    private Random rnd = new Random(System.currentTimeMillis());
    private final SimpleDateFormat timestamp = new SimpleDateFormat("yMMddHHmmss");


    {
        timestamp.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public UserServiceImpl(TransactionsRepository transactionsRepository, ManualRepository manualRepository, GatesRepository gatesRepository,
                           RestTemplate restTemplate, AccountsRepository accountsRepository, CounterRepository counterRepository) {
        this.transactionsRepository = transactionsRepository;
        this.manualRepository = manualRepository;
        this.gatesRepository = gatesRepository;
        this.restTemplate = restTemplate;
        this.accountsRepository = accountsRepository;
        this.counterRepository = counterRepository;
    }

    @Override
    public CompletableFuture<Transaction> withdraw(Transaction transaction) {
        if (manualRepository.findAndModifyAccountSub(transaction.getSenderCredentials().getAccountId()
                , transaction.getFinalAmount()) != null) {
            if (manualRepository.findAndModifyGateSub(transaction.getGateId(), transaction.getAmount()) != null) {
                Optional<Counter> counterOptional = counterRepository.findById(new ObjectId("5d52f5a28e2c7a1502118185"));
                Counter counter = null;

                if (counterOptional.isPresent())
                    counter = counterOptional.get();

                assert counter != null;
                transaction.setTransactionNumber(counter.getCount());

                counterRepository.save(counter.inc());
                transactionsRepository.insert(transaction);
                return CompletableFuture.completedFuture(transaction);
            } else {
                throw new NotEnoughMoneyException("Недостаточно средств на гейте или гейт с указанным ID не найден!");
            }
        } else {
            throw new NotEnoughMoneyException("Недостаточно средств на аккаунте или аккаунт с указанным ID не найден!");
        }
    }

    @Override
    public CompletableFuture<String> input(InitRequestData initRequestData, Long commission) {
        ReceiverCredentials receiverCredentials = ReceiverCredentials.builder()
                .accountId(new ObjectId(initRequestData.getAccountId()))
                .build();
        Optional<Account> accountOptional = accountsRepository.findById(receiverCredentials.getAccountId());
        if (accountOptional.isEmpty())
            throw new NotFoundException("Указанный аккаунт официанта не найден!");
        Gson gson = new Gson();
        List<Position> positions = new ArrayList<>(1);
        positions.add(Position.builder()
                .Price(Double.parseDouble(initRequestData.getAmount()) / 100)
                .build());
        Cheque cheque = Cheque.builder()
                .CheckClose(CheckClose.builder().build())
                .Positions(positions)
                .build();

        initRequestData.setCheque(Base64.getEncoder().encodeToString(gson.toJson(cheque).getBytes()));

        Account account = accountOptional.get();
        String accountCurrency = account.getBalance().getCurrency();

        Transaction transaction = Transaction.builder()
                .purpose(initRequestData.getDescription())
                .finalAmount(Balance.builder()
                        .amount(Long.parseLong(initRequestData.getAmount()) - commission)
                        .currency(accountCurrency)
                        .build())
                .amount(Balance.builder()
                        .amount(Long.valueOf(initRequestData.getAmount()))
                        .currency(accountCurrency)
                        .build())
                .commission(TransactionCommission.builder()
                        .systemAmount(0L)
                        .bankAmount(commission)
                        .currency("RUB")
                        .build())
                .type(DONATE)
                .startDate(new Date())
                .gateId(PaytureCredentials.PAYTURE_GATE_ID)
                .stage(0)
                .transactionNumber(System.currentTimeMillis())
                .receiverCredentials(receiverCredentials)
                .paytureLog(PaytureLog.builder().build())
                .cheque(cheque)
                .build();

        InitResponse initResponse = null;
        try {
            initResponse = initRequest(initRequestData);
        } catch (JAXBException exc) {
            transaction.getPaytureLog().setInitResponse(exc.getMessage());
            transactionsRepository.insert(transaction);
        }

        assert initResponse != null;
        if (initResponse.getSuccess().equals("True")) {
            transaction.setStatus(Status.WAITING);
            transaction.setOrderId(initResponse.getOrderId());
        } else {
            transaction.setStatus(Status.DENIED);
            transaction.setErrorReason(initResponse.getErrCode());
        }

        transaction.getPaytureLog().setInitRequest(initRequestData.toString());
        transaction.getPaytureLog().setInitResponse(initResponse.toString());
        transactionsRepository.insert(transaction);

        return CompletableFuture.completedFuture(initResponse.getSessionId().replace("\"", ""));
    }

    @Override
    public CompletableFuture<String> inputPsb(Transaction transaction) {
        final Optional<Gate> optionalGate = gatesRepository.findById(PSBCredentials.GATE_ID_DONATE);
        if (optionalGate.isEmpty())
            throw new NotFoundException("Гейт ПСБ не найден!");
        final Gate psb = optionalGate.get();

        if (accountsRepository.findById(transaction.getReceiverCredentials().getAccountId()).isEmpty()) {
            throw new NotFoundException("Аккаунт получателя не найден!");
        }
        transactionsRepository.save(transaction);

        LinkRequest inputRequest = LinkRequest.builder()
                .BACKREF(transaction.getSuccessRef())
                .EMAIL(transaction.getSenderCredentials().getSenderEmail())
                .DESC(transaction.getPurpose())
                .AMOUNT(String.valueOf(transaction.getFinalAmount().getAmount().doubleValue() / 100))
                .ORDER(transaction.getTransactionNumber().toString())
                .TERMINAL(psb.getTerminal())
                .build();
        inputRequest.createSign();

        return CompletableFuture.completedFuture(withdrawPsb(inputRequest, psb.getUrlPayment()).replace("\"", ""));
    }

    @Override
    public void getChequeData(Transaction transaction, CompletableFuture<DocumentState> completableFuture) {
        OrangeRequest orangeRequest = new OrangeRequest();
        orangeRequest.getDocument(transaction.getTransactionNumber().toString(), ZING_INN, new GetDocumentCallback() {
            @Override
            public void onSuccess(DocumentState documentState) {
                completableFuture.complete(documentState);
            }

            @Override
            public void onRequestFailure(Throwable throwable) {
                System.out.println(throwable.getMessage());
                completableFuture.complete(new DocumentState());
            }
        });
    }

    @Override
    public void generateCheque(Transaction transaction, CompletableFuture<DocumentState> completableFuture) {
        try {
            OrangeRequest orangeRequest = new OrangeRequest();
            orangeRequest.postDocument(getChequeRequest(transaction), KEYS_PATH + "private_key.der", new PostBillCallback() {
                @Override
                public void onSuccess() {
                    transactionsRepository.save(transaction);
                    completableFuture.complete(new DocumentState());
                }

                @Override
                public void onValidationErrors(@Nullable String[] errors) {
                    if (errors != null) {
                        for (String error : errors) {
                            System.out.println("onValidationErrors: " + error);
                        }
                    } else {
                        System.out.println("postResponse is null");
                    }

                    completableFuture.complete(new DocumentState());
                }

                @Override
                public void onRequestFailure(Throwable throwable) {
                    System.out.println("onRequestFailure " + throwable.getMessage());

                    completableFuture.complete(new DocumentState());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private RequestBody getChequeRequest(Transaction transaction) {
        Bill dummyBill = new Bill(
                1.0,
                transaction.getFinalAmount().getAmount().doubleValue() / 100,
                Tax.NONE,
                transaction.getPurpose());
        Payment dummyPayment = new Payment(
                PaymentType.CARD_MIR,
                transaction.getFinalAmount().getAmount().doubleValue() / 100
        );
        zing.externalLibs.src.models.CheckClose dummyCheckClose = new zing.externalLibs.src.models.CheckClose(
                Collections.singletonList(dummyPayment),
                TaxationSystem.OSN
        );

        String customerEmail = "1";
        /*if (transaction.getType().equals(DONATE)) {
            final var accountOptional = accountsRepository.findById(transaction.getReceiverCredentials().getAccountId());
            final var userOption
            customerEmail = ;
        }*/

        Document dummyDocument = new Document(
                DocumentType.INCOME,
                Collections.singletonList(dummyBill),
                dummyCheckClose,
                customerEmail
        );

        return new RequestBody(
                transaction.getTransactionNumber().toString(),
                ZING_INN,
                null,
                dummyDocument
        );
    }

    private String withdrawPsb(LinkRequest inputRequest, String url) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = null;
        try {
            map = inputRequest.requestData();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        HttpEntity<MultiValueMap<String, String>> entity =
                new HttpEntity<>(map, headers); //Подготовка запроса

        ResponseEntity<String> response =
                restTemplate.postForEntity(url, //Отправка запроса
                        entity,
                        String.class);

        Gson gson = new Gson();

        final LinkResponse linkResponse = gson.fromJson(response.getBody(), LinkResponse.class);
        return linkResponse.getREF().replace("\"", "");
    }

    private InitResponse initRequest(InitRequestData requestData) throws JAXBException {
        Optional<Gate> paytureGateOptional = gatesRepository.findById(PaytureCredentials.PAYTURE_GATE_ID);
        Gate gate;
        if (paytureGateOptional.isEmpty())
            throw new NotFoundException("Гейт payture не найден");
        gate = paytureGateOptional.get();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map =
                new LinkedMultiValueMap<>();
        map.add("Key", gate.getMerchantKey());
        String data;
        try {
            data = requestData.returnUrlEncoded();
            map.add("Data", data);
        } catch (IllegalAccessException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        HttpEntity<MultiValueMap<String, String>> entity =
                new HttpEntity<>(map, headers); //Подготовка запроса

        ResponseEntity<String> response =
                restTemplate.postForEntity(gate.getCurrentUrlInit(), //Отправка запроса
                        entity,
                        String.class);

        JAXBContext jaxbContext = JAXBContext.newInstance(InitResponse.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        StringReader reader = new StringReader(Objects.requireNonNull(response.getBody() //Анмаршеллинг ответа
                , StandardCharsets.UTF_8.toString()));


        return (InitResponse) jaxbUnmarshaller.unmarshal(reader);
    }
}
