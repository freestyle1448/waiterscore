package zing.services;

import com.mongodb.MongoException;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Transactional;
import zing.externalLibs.src.models.DocumentState;
import zing.models.payture.InitRequestData;
import zing.models.transaction.Transaction;

import java.util.concurrent.CompletableFuture;

@Async(value = "threadPoolTaskExecutor")
public interface UserService {
    @Retryable(
            value = {MongoException.class},
            maxAttempts = 100, backoff = @Backoff(50))
    @Transactional
    CompletableFuture<Transaction> withdraw(Transaction transaction);

    @Transactional
    CompletableFuture<String> input(InitRequestData initRequestData, Long commission);

    @Transactional
    CompletableFuture<String> inputPsb(Transaction transaction);

    void getChequeData(Transaction transaction, CompletableFuture<DocumentState> completableFuture);

    void generateCheque(Transaction transaction, CompletableFuture<DocumentState> completableFuture);
}
